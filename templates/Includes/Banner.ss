<!-- Includes/Banner -->
<div id="banner" class="banner" <% if SetHeroImageBanner %>style="background-image: url('$HeroImage.Link');"<% else_if $BannerImageLink %>style="background-image: url('$BannerImageLink');"<% end_if %> data-swiftype-index='false'>
	<div class="banner__overlay">&nbsp;</div>
	<div class="container">
		<div>
	        <% if $ClassName == 'HomePage' %>
	            <% include HomePopularLinks %>
	        <% end_if %>
	        <% if $ClassName == 'HomePage' %>
	            <% include BranchButtons %>
	        <% end_if %>
	        <% if $ClassName == 'HomePage' %>
	            <% include BranchButtons cssModifier='homePageContent' %>
	        <% end_if %>
	        <% if $ClassName != 'HomePage' %>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8">
						<h1>$Root.Title.XML</h1>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4">
						<% include QuickFinder %>
					</div>
				</div>
			<% end_if %>
		</div>
	</div>
</div>
